----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/14/2021 05:52:23 PM
-- Design Name: 
-- Module Name: i2c_master_o_c - rtl
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity i2c_master_o_c is
    Port ( scl : in STD_LOGIC;
           sda_i : out STD_LOGIC;
           sda_o : in STD_LOGIC;
           scl_o_c : out STD_LOGIC;
           sda_o_c : inout STD_LOGIC);
end i2c_master_o_c;

architecture rtl of i2c_master_o_c is

begin
  -- SCL: unidirectional out
  scl_o_c <= 'Z' when scl = '1' else '0';

  -- SDA: bidirectional
  sda_o_c <= 'Z' when sda_o = '1' else '0';
  sda_i <= sda_o_c;
end rtl;
