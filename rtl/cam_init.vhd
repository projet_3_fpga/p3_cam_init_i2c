----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/12/2021 01:18:48 PM
-- Design Name: 
-- Module Name: cam_init - rtl
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity cam_init is
    Port ( pwup : out STD_LOGIC;
           scl : out STD_LOGIC;
           sda_i : in STD_LOGIC;
           sda_o : out STD_LOGIC;

           clk_100MHz : in STD_LOGIC;
           rst : in STD_LOGIC;
           init_dn : out STD_LOGIC);
end cam_init;

architecture rtl of cam_init is
  -- 0x3C = 0x78 >> 1 ; la fiche technique de la PCAM-5C prête à confusion
  constant CAM_ADDR : std_logic_vector (6 downto 0) := "0111100";

begin
  
  -- Instantiation du master I2C
  i2c_master_inst : entity work.i2c_master
    port map (
      sda_i => sda_i,
      sda_o => sda_o,
      scl => scl,

      i2c_go => '1',
      i2c_dn => open,

      slave_addr => CAM_ADDR,
      r_w => '0',

      reg_addr => (others => '0'),
      data_wr => (others => '0'),
      data_rd => open,

      clk_100MHz => clk_100MHz,
      rst => rst
    );

  -- Instantiation de la MSA
  
  -- Instantiation des deux ROM
  
  -- Instantiation des trois timers 100ms 50ms 10ms
end rtl;
