----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/13/2021 08:57:49 PM
-- Design Name: 
-- Module Name: delay_signal - rtl
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity delay_signal is
  generic (
    CLK_FREQUENCY_HZ : integer := 100_000_000; -- 100 MHz
    DELAY_NS : integer := 200 -- 0.2μs
  );
  port (
    input   : in STD_LOGIC;
    delayed : out STD_LOGIC;

    clk : in STD_LOGIC;
    rst : in STD_LOGIC
  );
end delay_signal;

architecture rtl of delay_signal is
  constant N_DELAY_REGISTERS : integer := (CLK_FREQUENCY_HZ * DELAY_NS / 1e9);
  signal delay_registers : std_logic_vector (N_DELAY_REGISTERS-1 downto 0);
begin

shift_delay: process (clk, rst) is
begin
  if rising_edge(clk) then
    if (rst = '0') then
      delay_registers <= delay_registers(N_DELAY_REGISTERS-2 downto 0) & input;
    else
      delay_registers <= (others => '0');
    end if;
    delayed <= delay_registers(N_DELAY_REGISTERS-1);
  end if;

end process shift_delay;

end rtl;
