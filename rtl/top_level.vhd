----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/01/2021 06:06:02 PM
-- Design Name: 
-- Module Name: top_level - rtl
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity top_level is
  port (
    P13_18        : out std_logic;
    P13_22        : out std_logic;
    P13_24        : inout std_logic;
    LED_0         : out std_logic; -- logique positive
    RESET         : in std_logic; -- reset button
    CLK1          : in std_logic -- 100 MHz quartz
  );
end top_level;

architecture rtl of top_level is

  component clk_wiz_0
  port (
    clk_in : in std_logic;
    clk_out : out std_logic;
    reset : in std_logic;
    locked : out std_logic
  );
  end component;

  -- I2C
  signal scl : std_logic;
  signal scl_o_c : std_logic;

  signal sda_i : std_logic;
  signal sda_o : std_logic;
  signal sda_o_c : std_logic;

  -- Clock management
  signal clk_mmcm : std_logic;
  signal clk_valid : std_logic;
  signal clk_100MHz : std_logic;

begin

  -- Broches SCL & SDA open-drain
  i2c_master_o_c_inst: entity work.i2c_master_o_c
    port map (
      scl   => scl,
      sda_i => sda_i,
      sda_o => sda_o,

      scl_o_c => P13_22,
      sda_o_c => P13_24
    );

  -- Instantiation du cam_init
  cam_init: entity work.cam_init
    port map (
      pwup        => P13_18,
      scl         => scl,
      sda_i       => sda_i,
      sda_o       => sda_o,
      clk_100MHz  => clk_100MHz,
      rst          => clk_valid,
      init_dn     => LED_0
    );

  -- Clock wizard
  sys_clk : clk_wiz_0
    port map (
      clk_in  => CLK1,
      clk_out => clk_mmcm,
      reset => RESET,
      locked => clk_valid
    );

  -- Distribution d'horloge
  BUFG_inst : BUFG
    port map (
       O => clk_100MHz,
       I => clk_mmcm
    );

end rtl;
