----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/12/2021 12:32:43 PM
-- Design Name: 
-- Module Name: cam_init_msa - rtl
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity cam_init_msa is
    Port ( clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           data_rd : in STD_LOGIC_VECTOR (7 downto 0);
           i2c_go : out STD_LOGIC;
           i2c_dn : in STD_LOGIC;
           i2c_r_w : in STD_LOGIC;
           data_wr : out STD_LOGIC_VECTOR (7 downto 0);
           reg_addr : out STD_LOGIC_VECTOR (15 downto 0);
           rom_reg_addr : in STD_LOGIC_VECTOR (15 downto 0);
           rom_reg_content : in STD_LOGIC_VECTOR (7 downto 0);
           rom_cnt : out STD_LOGIC_VECTOR (7 downto 0);
           init_dn : in STD_LOGIC);
end cam_init_msa;

architecture rtl of cam_init_msa is

  -- IFL
  
  -- OFL
  
  -- Processus registré
  
begin


end rtl;
