----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/12/2021 02:47:22 PM
-- Design Name: 
-- Module Name: i2c_master_clk - rtl
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity i2c_master_clk is
  generic (
    CLK_SPEED_IN_HZ : integer := 100_000_000; -- 100 MHz
    CLK_SPEED_OUT_HZ : integer := 2*400_000     -- 2 * (I2C low speed 100kHz ; high speed 400kHz)
  );
  Port ( clk_in : in STD_LOGIC;
         clk_out : out STD_LOGIC;
         rst : in STD_LOGIC);
end i2c_master_clk;

architecture rtl of i2c_master_clk is
  constant threshold : integer := (CLK_SPEED_IN_HZ / CLK_SPEED_OUT_HZ) / 2;
  signal count : integer range 0 to threshold;
  
begin

  counter : process(clk_in, rst) is
  begin
    if rising_edge(clk_in) then
      if (rst = '0') then
        if (count = threshold) then
          clk_out <= not clk_out;
          count <= 1;
        else
          clk_out <= clk_out;
          count <= count + 1;
        end if;
      else
        clk_out <= '0';
        count <= 0;
      end if;
    end if;    
  end process counter;

end rtl;
