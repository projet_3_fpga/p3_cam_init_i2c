----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/12/2021 01:52:27 PM
-- Design Name: 
-- Module Name: cam_init_tb - stimulus
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity cam_init_tb is
--  Port ( );
end cam_init_tb;

architecture stimulus of cam_init_tb is
  constant CLK_PERIOD : time := 10ns;
  
  signal pwup : std_logic;
  signal scl : std_logic;
  signal sda : std_logic;
  signal clk_100MHz : std_logic;
  signal en : std_logic;
  signal error : std_logic;
  signal init_dn : std_logic;
begin

  -- Instantiate DUT
  DUT: entity work.cam_init
    port map (
      pwup        => pwup,
      scl         => scl,
      sda         => sda,
      clk_100MHz  => clk_100MHz,
      en          => en,
      error       => error,
      init_dn     => init_dn
    );
   
  -- en
  en <= '0', '1' after 2*CLK_PERIOD;
     
  -- Drive clock
  drive_clk : process is
  begin
    clk_100MHz <= '0';
    wait for CLK_PERIOD / 2;
    clk_100MHz <= '1';
    wait for CLK_PERIOD / 2;
  end process drive_clk;
  
  -- Just let it run and check output waveforms
  -- should quit after attempting to read sensor ID registers

end stimulus;
