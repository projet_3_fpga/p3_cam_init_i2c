----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/14/2021 05:53:39 PM
-- Design Name: 
-- Module Name: i2c_master_o_c_tb - stimulus
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity i2c_master_o_c_tb is
end i2c_master_o_c_tb;

architecture stimulus of i2c_master_o_c_tb is
  signal scl : STD_LOGIC;
  signal sda_i : STD_LOGIC;
  signal sda_o : STD_LOGIC;
  signal scl_o_c : STD_LOGIC;
  signal sda_o_c : STD_LOGIC;
begin

  -- Instantiate DUT
  DUT: entity work.i2c_master_o_c
    port map (
      scl => scl,
      sda_i => sda_i,
      sda_o => sda_o,
      scl_o_c => scl_o_c,
      sda_o_c => sda_o_c
    );

  test_bench: process is
  begin
    -- Drive both to '0'
    scl <= '0';
    sda_o <= '0';
    sda_o_c <= 'Z';
    -- O/C outputs should be '0'
    wait for 10ns;
    
    -- Drive both to '1'
    scl <= '1';
    sda_o <= '1';
    sda_o_c <= 'Z';
    -- O/C outputs should be 'Z'
    wait for 10ns;
    
    -- Drive sda_i to '0' as an external device would while sda_o_c is High-Z
    sda_o_c <= '0';
    -- SDA input should be '0'
    wait for 10ns;

    -- Drive sda_i to '1' as an external device would while sda_o_c is High-Z
    sda_o_c <= '1';
    -- SDA input should be '1'
    wait;
  end process test_bench;

end stimulus;
