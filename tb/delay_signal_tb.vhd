----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/14/2021 11:55:42 AM
-- Design Name: 
-- Module Name: delay_signal_tb - stimulus
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity delay_signal_tb is
end delay_signal_tb;

architecture stimulus of delay_signal_tb is
  constant CLK_PERIOD : time := 10 ns;
  
  -- Define DUT signal copies
  signal input   : STD_LOGIC;
  signal delayed : STD_LOGIC;

  signal clk : STD_LOGIC;
  signal rst : STD_LOGIC;
begin
  
  -- Instantiate DUT
  DUT: entity work.delay_signal
    generic map (
      CLK_FREQUENCY_HZ => 100_000_000, -- 100 MHz
      DELAY_NS => 100 -- 0.1μs
    )
    port map (
      input   => input,
      delayed => delayed,
  
      clk => clk,
      rst => rst
    );
    
  -- Rst
  rst <= '1', '0' after 2*CLK_PERIOD;

  -- Drive clock
  drive_clk : process is
  begin
    clk <= '0';
    wait for CLK_PERIOD / 2;
    clk <= '1';
    wait for CLK_PERIOD / 2;
  end process drive_clk;

  test_bench: process is
  begin
    input <= '0';
    wait for 2*CLK_PERIOD;
    
    -- Send sucessive pulses: "1010 0001"
    input <= '1';
    wait for CLK_PERIOD;
    
    input <= '0';
    wait for CLK_PERIOD;
    
    input <= '1';
    wait for CLK_PERIOD;
    
    input <= '0';
    wait for 4*CLK_PERIOD;

    input <= '1';
    wait for CLK_PERIOD;
    input <= '0';
    
    wait;
    -- Check that they come out with the precribed delay
  end process test_bench;   

end stimulus;
