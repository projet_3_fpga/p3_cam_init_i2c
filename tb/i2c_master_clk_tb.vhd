----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/12/2021 02:53:43 PM
-- Design Name: 
-- Module Name: i2c_master_clk_tb - stimulus
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity i2c_master_clk_tb is
end i2c_master_clk_tb;

architecture stimulus of i2c_master_clk_tb is
  constant CLK_PERIOD : time := 10ns;
  
  signal clk_in : std_logic;
  signal clk_out : std_logic;
  signal rst : std_logic;
begin

  DUT: entity work.i2c_master_clk(rtl)
    port map (
      clk_in => clk_in,
      clk_out => clk_out,
      rst => rst
    );
  
  drive_clk : process is
  begin
    clk_in <= '0';
    wait for CLK_PERIOD / 2;
    clk_in <= '1';
    wait for CLK_PERIOD / 2;
  end process drive_clk;
  
  rst <= '1', '0' after 2*CLK_PERIOD;
  
end stimulus;
